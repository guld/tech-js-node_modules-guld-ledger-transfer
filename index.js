class Transfer extends ledgerTypes.Transaction {
  static create (sender, recipient, amount, commodity, time) {
    time = time || Math.trunc(Date.now() / 1000)
    var date = new Date(time * 1000)
    var datestr = `${date.toISOString().split('T')[0].replace(/-/g, '/')}`
    return new Transfer(`${datestr} * transfer
    ; timestamp: ${time}
    ${sender}:Assets   -${amount} ${commodity}
    ${sender}:Expenses   ${amount} ${commodity}
    ${recipient}:Assets   ${amount} ${commodity}
    ${recipient}:Income   -${amount} ${commodity}
`)
  }
}

module.exports = Transfer
