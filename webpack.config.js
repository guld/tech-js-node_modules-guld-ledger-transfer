module.exports = [
  {
    target: 'web',
    entry: {
      index: './index.js'
    },
    output: {
      filename: 'guld-ledger-transfer.min.js',
      path: __dirname,
      library: 'LedgerTransfer',
      libraryTarget: 'var'
    }
  }
]
