# guld-ledger-transfer

[![source](https://img.shields.io/badge/source-bitbucket-blue.svg)](https://bitbucket.org/guld/tech-js-node_modules-guld-ledger-transfer) [![issues](https://img.shields.io/badge/issues-bitbucket-yellow.svg)](https://bitbucket.org/guld/tech-js-node_modules-guld-ledger-transfer/issues) [![documentation](https://img.shields.io/badge/docs-guld.tech-green.svg)](https://guld.tech/lib/guld-ledger-transfer.html)

[![node package manager](https://img.shields.io/npm/v/guld-ledger-transfer.svg)](https://www.npmjs.com/package/guld-ledger-transfer) [![travis-ci](https://travis-ci.org/guldcoin/tech-js-node_modules-guld-ledger-transfer.svg)](https://travis-ci.org/guldcoin/tech-js-node_modules-guld-ledger-transfer?branch=guld) [![lgtm](https://img.shields.io/lgtm/grade/javascript/b/guld/tech-js-node_modules-guld-ledger-transfer.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/guld/tech-js-node_modules-guld-ledger-transfer/context:javascript) [![david-dm](https://david-dm.org/guldcoin/tech-js-node_modules-guld-ledger-transfer/status.svg)](https://david-dm.org/guldcoin/tech-js-node_modules-guld-ledger-transfer) [![david-dm](https://david-dm.org/guldcoin/tech-js-node_modules-guld-ledger-transfer/dev-status.svg)](https://david-dm.org/guldcoin/tech-js-node_modules-guld-ledger-transfer?type=dev)

Guld ledger transfer generic.

### Install

##### Node

```sh
npm i guld-ledger-transfer
```

### Usage

```
LedgerTransfer.create('sender', 'receipient', 1.1, 'GULD')

XXXX/XX/XX * transfer
    ; timestamp: xxxxxxxxxxx
    sender:Assets   -1.1 GULD
    sender:Expenses   1.1 GULD
    recipient:Assets   1.1 GULD
    recipient:Income   -1.1 GULD

```

##### Node

```
const LedgerTransfer = require('guld-ledger-transfer')
```

### License

MIT Copyright isysd
