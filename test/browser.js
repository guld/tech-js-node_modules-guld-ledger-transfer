/* global describe:false it:false */
const chai = require('chai')

describe('transfer', function () {
  it('global available', function () {
    chai.assert.exists(LedgerTransfer)
  })
  it('create', function () {
    var t = LedgerTransfer.create('sender', 'recipient', 1.1, 'GULD')
    chai.assert.typeOf(t, ledgerTypes.Transaction)
    chai.assert.typeOf(t, LedgerTransfer)
    console.log(t)
    console.log(t.raw)
    var re = `.* \* transfer
    ; timestamp: [0-9]*
    sender:Assets   -1.1 GULD
    sender:Expenses   1.1 GULD
    recipient:Assets   1.1 GULD
    recipient:Income   -1.1 GULD
`
    chai.assert.isTrue(new RegExp(re).test(t.raw))
  })
})
